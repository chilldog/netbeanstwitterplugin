/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.factories;

import com.pwlsoft.pawl.tweet.domain.TweetVO;
import com.pwlsoft.pawl.tweet.service.TwitterService;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.lookup.ServiceProvider;
import twitter4j.User;

/**
 *
 * @author piotr.pawliszcze
 */
@ServiceProvider(service = ImageRepository.class)

public class ImageRepository implements PropertyChangeListener {

    private Map<Long, ImageIcon> profileImagesMap = new HashMap<>();

    private Map<Long, ImageIcon> tweetImagesMap = Collections.emptyMap();

    private static final Logger LOG = Logger.getLogger(ImageRepository.class.getName());

    /**
     * Default constructor
     */
    public ImageRepository() {
        //update image map from files on disk

    }

    /**
     * Method return instance of ImageRepository Service.
     *
     * @return {@link ImageRepository}
     */
    public static ImageRepository getinstance() {
        return Lookup.getDefault().lookup(ImageRepository.class);
    }

    /**
     *
     * @param profileImageURL
     * @return
     */
    public static ImageIcon getImageIconFromURL(String profileImageURL) {
        try {
            URL url = new URL(profileImageURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Image image = ImageIO.read(input);
            connection.disconnect();
            return new ImageIcon(image);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(TwitterService.PROPERTY_NAME_TWEET_LIST)) {
            try {
                List<TweetVO> tweetList = (List<TweetVO>) evt.getNewValue();
                SwingUtilities.invokeAndWait(() -> {
                    for (TweetVO tweetVO : tweetList) {
                        // update map of users
                        User user = tweetVO.getTweet().getUser();
                        if (!profileImagesMap.containsKey(user.getId())) {
                            String profileImageURL = user.getProfileImageURL();
                            ImageIcon profileIcon = getImageIconFromURL(profileImageURL);
                            profileImagesMap.put(user.getId(), profileIcon);
                        }

                        if (tweetVO.getTweet().isRetweet()) {
                            User retweetedUser = tweetVO.getTweet().getRetweetedStatus().getUser();
                            if (!profileImagesMap.containsKey(retweetedUser.getId())) {
                                String retweetedProfileImageURL = retweetedUser.getProfileImageURL();
                                ImageIcon retweetedIconPicture = getImageIconFromURL(retweetedProfileImageURL);
                                profileImagesMap.put(retweetedUser.getId(), retweetedIconPicture);
                            }
                        }
                    }
                });
            } catch (InterruptedException ex) {
                Exceptions.printStackTrace(ex);
            } catch (InvocationTargetException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        saveData();
    }

    public void loadData() {
        ObjectInputStream in = null;
        try {
            if (NbPreferences.forModule(ImageRepository.class).getByteArray("imagesMap", null) != null) {
                byte[] byteArray = NbPreferences.forModule(ImageRepository.class).getByteArray("imagesMap", null);
                ByteArrayInputStream byteIn = new ByteArrayInputStream(byteArray);
                in = new ObjectInputStream(byteIn);
                profileImagesMap = (Map<Long, ImageIcon>) in.readObject();
                if (profileImagesMap != null) {
                    LOG.log(Level.SEVERE, "profile image map loaded with : " + profileImagesMap.size());
                }
            }

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } catch (ClassNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    public void saveData() {
        SwingUtilities.invokeLater(() -> {
            ObjectOutputStream out = null;
            try {
                ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
                out = new ObjectOutputStream(byteOut);
                out.writeObject(profileImagesMap);
                NbPreferences.forModule(ImageRepository.class).putByteArray("imagesMap", byteOut.toByteArray());
                LOG.log(Level.SEVERE, "profile image map stored, size : " + profileImagesMap.size());
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            } finally {
                try {
                    out.close();
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        });
    }

    /**
     * 
     * @return 
     */
    public Map<Long, ImageIcon> getProfileImagesMap() {
        return profileImagesMap;
    }

}
