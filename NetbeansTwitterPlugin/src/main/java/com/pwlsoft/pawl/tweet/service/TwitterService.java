/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.service;

import com.pwlsoft.pawl.tweet.domain.TweetVO;
import com.pwlsoft.pawl.tweet.options.PawlUserSettingsPanel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import static java.util.concurrent.TimeUnit.SECONDS;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import twitter4j.IDs;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

/**
 *
 * @author piotr.pawliszcze
 */
@ServiceProvider(service = TwitterService.class)
public class TwitterService {

    public final static String PROPERTY_NAME_TWEET_LIST = "TWEET_LIST";

    private static final Logger LOGGER = Logger.getLogger(TwitterService.class.getName());
    private final String consumerKey;
    private final String consumerKeySecret;
    private final String accessToken;
    private final String accessTokenSecret;
    private final Twitter twitter = new TwitterFactory().getInstance();
    private final Set<Long> tweetList;
    private IDs followersIDs;
    private IDs followedIDs;
    private final PropertyChangeSupport pcs;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    /**
     * Add property change listener for update service
     *
     * @param listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    /**
     * Method return instance of Twitter Service.
     *
     * @return {@link TwitterService}
     */
    public static TwitterService getinstance() {
        return Lookup.getDefault().lookup(TwitterService.class);
    }

    /**
     * default constructor
     */
    public TwitterService() {
        this.tweetList = new HashSet<>();
        this.pcs = new PropertyChangeSupport(this);
        consumerKey = Preferences.userNodeForPackage(PawlUserSettingsPanel.class).get(PawlUserSettingsPanel.CONSUMER_KEY, "");
        consumerKeySecret = Preferences.userNodeForPackage(PawlUserSettingsPanel.class).get(PawlUserSettingsPanel.CONSUMER_KEY_SECRET, "");
        if (consumerKey.isEmpty() || consumerKeySecret.isEmpty()) {
            StatusDisplayer.getDefault().setStatusText("Missing login details -> Options -> P@wl");
        }
        this.twitter.setOAuthConsumer(consumerKey, consumerKeySecret);
        LOGGER.info("Autentycation OK");
        accessToken = Preferences.userNodeForPackage(PawlUserSettingsPanel.class).get(PawlUserSettingsPanel.SAVED_ACCESS_TOKEN, "");
        accessTokenSecret = Preferences.userNodeForPackage(PawlUserSettingsPanel.class).get(PawlUserSettingsPanel.SAVED_ACCESS_TOKEN_SECRET, "");
        AccessToken oathAccessToken = new AccessToken(accessToken, accessTokenSecret);
        if (accessToken.isEmpty() || accessTokenSecret.isEmpty()) {
            StatusDisplayer.getDefault().setStatusText("Missing token details -> Options -> P@wl");
        }
        twitter.setOAuthAccessToken(oathAccessToken);
        LOGGER.info("Connection OK");
    }

    /**
     * Method will add tweet by ID to favorites
     *
     * @param tweetId {@code long}
     * @return updated Tweet with favorites status
     */
    public Status createFavorite(long tweetId) {
        try {
//            String accessToken = getSavedAccessToken();
//            String accessTokenSecret = getSavedAccessTokenSecret();
//            AccessToken oathAccessToken = new AccessToken(accessToken, accessTokenSecret);
//            twitter.setOAuthAccessToken(oathAccessToken);
            return twitter.createFavorite(tweetId);
        } catch (TwitterException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    /**
     * Method will remove teet from favorites by tweet ID
     *
     * @param tweetId {@code long} removed tweet message ID
     * @return {@link Status} updated tweet with favorites status
     */
    public Status destroyFavorite(long tweetId) {
        try {
            return twitter.destroyFavorite(tweetId);
        } catch (TwitterException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    /**
     * Method update tweet list latest 20 tweet list from server
     *
     */
    public void updateTweetList() {
        try {

            List<TweetVO> latestTweetList = new ArrayList<>();
            twitter.getHomeTimeline().stream().forEach((tweet) -> {
                TweetVO twitMessage = new TweetVO(tweet);
                twitMessage.setIsNew(!tweetList.contains(tweet.getId()));
                latestTweetList.add(twitMessage);
                tweetList.add(tweet.getId());
            });

            pcs.firePropertyChange(PROPERTY_NAME_TWEET_LIST, tweetList, latestTweetList);
            LOGGER.log(Level.INFO, "Tweet list updated :{0}", tweetList.size());

        } catch (TwitterException ex) {
            Exceptions.printStackTrace(ex);
            //ToDO
            throw new RuntimeException(ex);
        }

    }

    /**
     * Method return updated array of follower IDs
     *
     * @return {@link ID}
     */
    public IDs getFollowersIDs() {
        return followersIDs;
    }

    /**
     * Method return updated array of followed users IDs
     *
     * @return {@link ID}
     */
    public IDs getFollowedIDs() {
        return followedIDs;
    }

    /**
     * Method create scheduled thread
     */
    public void runTwitterUpdateService() {
        final Runnable twitcheckService = () -> {
            LOGGER.log(Level.INFO, "Update service status : STARTED");
            updateFollowersList();
            updateFollowedList();
            updateTweetList();
            LOGGER.log(Level.INFO, "Update service status : STOPED");
        };

        SwingUtilities.invokeLater(() -> {
            scheduler.scheduleAtFixedRate(twitcheckService, 1, 61, SECONDS);
            scheduler.schedule(() -> {
            }, 5, TimeUnit.DAYS);
        });

    }

    /**
     * Update followed list in separate thread
     */
    private void updateFollowedList() {
        //TODO temporary
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {

                    followedIDs = twitter.getFriendsIDs(-1);
                    LOGGER.log(Level.INFO, "Followed list updated :{0}", followedIDs.getIDs().length);
                } catch (TwitterException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        });
    }

    /**
     * update followers list in separate thread
     */
    private void updateFollowersList() {
        //temporary
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    followersIDs = twitter.getFollowersIDs(-1);
                    LOGGER.log(Level.INFO, "Followers list updated :{0}", followersIDs.getIDs().length);
                } catch (TwitterException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        });
    }


}
