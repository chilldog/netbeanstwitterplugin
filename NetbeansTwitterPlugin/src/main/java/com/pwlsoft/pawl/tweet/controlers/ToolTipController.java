/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.controlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.Timer;
import net.java.balloontip.BalloonTip;
import net.java.balloontip.CustomBalloonTip;

/**
 *
 * @author piotr.pawliszcze
 */
public class ToolTipController extends MouseAdapter implements MouseMotionListener {

    private final BalloonTip balloonTip;
    private final Timer initialTimer;
    private final Timer showTimer;

    /**
     * Constructor
     *
     * @param balloonTip	the balloon tip to turn into a tooltip
     * @param initialDelay	in milliseconds, how long should you hover over the
     * attached component before showing the tooltip
     * @param showDelay	in milliseconds, how long should the tooltip stay
     * visible
     */
    public ToolTipController(final BalloonTip balloonTip, int initialDelay, int showDelay) {
        super();
        this.balloonTip = balloonTip;
        initialTimer = new Timer(initialDelay, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                balloonTip.setVisible(true);
                showTimer.start();
                stopTimers();
                
            }
        });
        initialTimer.setRepeats(false);

        showTimer = new Timer(showDelay, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                balloonTip.setVisible(false);
            }
        });
        showTimer.setRepeats(false);
    }

    public void mouseEntered(MouseEvent e) {
        initialTimer.start();
    }

    public void mouseMoved(MouseEvent e) {
        if (balloonTip instanceof CustomBalloonTip) {
            // If the mouse is within the balloon tip's attached rectangle
            if (((CustomBalloonTip) balloonTip).getOffset().contains(e.getPoint())) {
                if (!balloonTip.isVisible() && !initialTimer.isRunning()) {
                    initialTimer.start();
                }
            } else {
                stopTimers();
                balloonTip.setVisible(false);
            }
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        stopTimers();
        balloonTip.setVisible(false);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        stopTimers();
        balloonTip.setVisible(false);
    }

    /*
     * Stops all timers related to this tool tip
     */
    private void stopTimers() {
        initialTimer.stop();
        showTimer.stop();
    }

}
