/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.lookupProvider;

import org.openide.util.ContextGlobalProvider;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author piotr.pawliszcze
 */
@ServiceProvider(service = ContextGlobalProvider.class,
        //this next arg is nessesary if you want yours to be the default
        supersedes = "org.netbeans.modules.openide.windows.GlobalActionContextImpl")
public class CentralLookup implements ContextGlobalProvider {

    private final InstanceContent content = new InstanceContent();
    private final Lookup lookup = new AbstractLookup(content);

    public CentralLookup() {
    }

    public void add(Object instance) {
        content.add(instance);
    }

    public void remove(Object instance) {
        content.remove(instance);
    }

    public static CentralLookup getInstance() {
        return CentralLookupHolder.INSTANCE;
    }

    @Override
    public Lookup createGlobalContext() {
        return lookup;
    }

    private static class CentralLookupHolder {
        private static final CentralLookup INSTANCE = Lookup.getDefault().lookup(CentralLookup.class);
    }
}
