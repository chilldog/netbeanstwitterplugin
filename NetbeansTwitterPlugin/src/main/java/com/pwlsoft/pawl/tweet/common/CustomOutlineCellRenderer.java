/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.common;

/**
 *
 * @author piotr.pawliszcze
 */
import com.pwlsoft.pawl.tweet.domain.TweetVO;
import com.pwlsoft.pawl.tweet.outlineview.TwitterCellMessagePanel;
import com.pwlsoft.pawl.tweet.outlineview.SelectedTwitterCellMessagePanel;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Renderer used to remove the property editor button and the grey appearance of
 * the cells of an outline view. It uses the <@link PropertyTextRenderer/&gt; of
 * the property if any was set.
 */
public class CustomOutlineCellRenderer implements ListCellRenderer {

    public CustomOutlineCellRenderer(ListCellRenderer delegate) {
        this.delegate = delegate;

    }
    private final ListCellRenderer delegate;
    private int height = -1;

//    @Override
//    @SuppressWarnings("unchecked")
//    public Component getTableCellRendererComponent(final JTable table,
//            final Object value,
//            final boolean isSelected,
//            final boolean hasFocus,
//            final int row,
//            final int column) {
//
//        if (value != null) {
//            Object twettnode = table.getModel().getValueAt(row, column);
//            TwettMessageNode node = (TwettMessageNode) Visualizer.findNode(twettnode);
//            TwitterCellMessagePanel panel = new TwitterCellMessagePanel(node.getTwetMessage());
//            return panel;
//        }
//
//        return null;
//    }
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (value != null) {
            TweetVO twettnode = (TweetVO) list.getModel().getElementAt(index);

            if (isSelected) {
                TwitterCellMessagePanel twitterCellMessagePanel = new TwitterCellMessagePanel(twettnode);
                Dimension size = twitterCellMessagePanel.getPreferredSize();
                size.height = 70;
                twitterCellMessagePanel.setPreferredSize(size);
                return twitterCellMessagePanel;
            } else {
                SelectedTwitterCellMessagePanel twitterCellMessagePanel = new SelectedTwitterCellMessagePanel(twettnode);
                Dimension size = twitterCellMessagePanel.getPreferredSize();
                size.height = 150;
                twitterCellMessagePanel.setPreferredSize(size);
                return twitterCellMessagePanel;
            }
            
        }
        return null;
    }

}
