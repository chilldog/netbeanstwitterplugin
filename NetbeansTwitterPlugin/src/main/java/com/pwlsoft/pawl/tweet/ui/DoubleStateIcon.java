/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Objects;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JToggleButton;

/**
 *
 * @author piotr.pawliszcze
 */
public abstract class DoubleStateIcon extends JToggleButton {

    public static final String PROP_SELECTED = "selected";
    public static final String ICON_PREFIX = "/com/pwlsoft/pawl/twit/icons/";
    private static final long serialVersionUID = 1L;

    private Icon selectedIcon;
    private Icon rolloverIcon;
    private Icon defaultIcon;
    private static final Logger LOG = Logger.getLogger(DoubleStateIcon.class.getName());

    /**
     * Get the value of defaultIcon
     *
     * @return the value of defaultIcon
     */
    public Icon getDefaultIcon() {
        return defaultIcon;
    }

    /**
     * Set the value of defaultIcon
     *
     * @param defaultIcon new value of defaultIcon
     */
    public void setDefaultIcon(Icon defaultIcon) {
        this.defaultIcon = defaultIcon;
    }

    public DoubleStateIcon(String label, ActionButtonType iconName, boolean isSelected) {
        componentInit();
        setSelected(isSelected);

        URL defaultIconURL = getClass().getResource(ICON_PREFIX + iconName + ".png");
        if (Objects.nonNull(defaultIconURL)) {
            this.defaultIcon = new javax.swing.ImageIcon(defaultIconURL);
            setIcon(defaultIcon);
        }

        URL selectedIconURL = getClass().getResource(ICON_PREFIX + iconName + "_on.png");
        if (Objects.nonNull(selectedIconURL)) {
            this.selectedIcon = new javax.swing.ImageIcon(selectedIconURL);
            setSelectedIcon(selectedIcon);
        }

        URL hoveredIconURL = getClass().getResource(ICON_PREFIX + iconName + "_hover.png");
        if (Objects.nonNull(hoveredIconURL)) {
            this.rolloverIcon = new javax.swing.ImageIcon(hoveredIconURL);
            setRolloverEnabled(true);
            setRolloverIcon(rolloverIcon);
        }

        initiIcon(label);
    }

    private void componentInit() {
        setFocusable(false);
        setBorder(null);
        setContentAreaFilled(false);
        setBackground(new java.awt.Color(51, 51, 51));
        setFont(new java.awt.Font("Consolas", 0, 10)); // NOI18N
        setForeground(new java.awt.Color(155, 167, 175));
        setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        this.addActionListener(new DoubleStateButtonAction());
    }

    private void initiIcon(String label) {
        String parameter = label;
        String prefix = "   ";
        if (parameter.equals("0")) {
            parameter = "";
        }
        String substring = "";
        if (parameter.length() < 4) {
            substring = prefix.substring(0, prefix.length() - parameter.length());
        }
        setText(parameter + substring);
    }

    abstract public void click(ActionEvent e);

    private class DoubleStateButtonAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            click(e);
        }
    }

}
