/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.actions;

import static com.pwlsoft.pawl.tweet.actions.CreateFavoriteAction.ACTION_ID;
import com.pwlsoft.pawl.tweet.domain.TweetVO;
import com.pwlsoft.pawl.tweet.service.TwitterService;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import twitter4j.Status;

@ActionID(
        category = "Twitter/FavoriteAction",
        id = ACTION_ID
)
@ActionRegistration(
        iconBase = "com/pwlsoft/pawl/twit/icons/favorite.png",
        displayName = "#CTL_CreateFavoriteAction"
)
@ActionReferences({
    @ActionReference(path = "Menu/File", position = 1300),
    @ActionReference(path = "Toolbars/File", position = 300)
})
@Messages("CTL_CreateFavoriteAction=add to favorities")
public final class CreateFavoriteAction extends AbstractAction {

    public static final String ACTION_ID = "com.pwlsoft.pawl.twit.actions.CreateFavoriteAction";

    public static final String TWEET_VALUE = "tweet";

    private final TweetVO tweet;

    private final TwitterService twitterService = Lookup.getDefault().lookup(TwitterService.class);

    private static final Logger logger = Logger.getLogger(CreateFavoriteAction.class.getName());

    public CreateFavoriteAction(TweetVO tweet) {
        this.tweet = tweet;
        System.out.println(" odpalam akcje:"+tweet.getTweet().getUser().getName());
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        Status setFavorite = setFavorite(tweet.getTweet());
        putValue(TWEET_VALUE, setFavorite);
    }

    public Status setFavorite(Status tweet) {
        Status updatetTweet;
        if (tweet.isFavorited()) {
            updatetTweet = twitterService.destroyFavorite(tweet.getId());
            logger.log(Level.INFO, "tweet from :{0} removed from Favorities", tweet.getUser().getName());
        } else {
            updatetTweet = twitterService.createFavorite(tweet.getId());
            logger.log(Level.INFO, "tweet from :{0} added to Favorities", tweet.getUser().getName());
        }
        this.tweet.setTweet(updatetTweet);
        return tweet;
    }

}
