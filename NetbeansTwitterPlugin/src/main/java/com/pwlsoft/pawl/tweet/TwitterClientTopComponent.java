/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet;

import com.pwlsoft.pawl.tweet.domain.TweetVO;
import com.pwlsoft.pawl.tweet.factories.ImageRepository;
import com.pwlsoft.pawl.tweet.outlineview.SelectedTwitterCellMessagePanel;
import com.pwlsoft.pawl.tweet.service.TwitterService;
import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;
import twitter4j.TwitterException;

/**
 * Top component which displays something.
 */
@TopComponent.Description(
        preferredID = TwitterClientTopComponent.PRFERRED_ID,
        persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "output", openAtStartup = true)
@ActionID(category = "Window", id = " com.pwlsoft.pawl.tweet.TwitTopComponent")
@ActionReference(path = "Menu/Window" /*, position = 333 */)

@TopComponent.OpenActionRegistration(
        displayName = "#CTL_MainWindowTwitAction",
        preferredID = TwitterClientTopComponent.PRFERRED_ID
)
@Messages({
    "CTL_MainWindowTwitAction=Twit",
    "CTL_MainWindowTwitTopComponent=P@WL Twitter Client",
    "HINT_MainWindowTwitTopComponent=This is a Twit window"
})
public class TwitterClientTopComponent extends TopComponent implements PropertyChangeListener {

    public final static String PRFERRED_ID = "MainWindowTwitTopComponent";
    private static final long serialVersionUID = 1L;
    private final TwitterService twitterService;
    private final JScrollPane listScroller = new JScrollPane();
    private final JPanel messagePanel = new JPanel();
    private final ImageRepository imageRepository;
    private static final Logger LOG = Logger.getLogger(TwitterClientTopComponent.class.getName());
    

    
    public TwitterClientTopComponent() throws TwitterException, InterruptedException, ExecutionException {
        initComponents();
        setPreferredSize(new java.awt.Dimension(104, 200));
        twitterService = TwitterService.getinstance();
        //initializing image repository
        imageRepository = ImageRepository.getinstance();
        twitterService.addPropertyChangeListener(imageRepository);        
        twitterService.addPropertyChangeListener(this);

        messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.PAGE_AXIS));

        add(listScroller, BorderLayout.CENTER);
        listScroller.setViewportView(messagePanel);
        listScroller.validate();
        listScroller.repaint();

    }

    /**
     * This Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.GridLayout(1, 0));
        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

    @Override
    protected void componentOpened() {
        super.componentOpened(); //To change body of generated methods, choose Tools | Templates.
        twitterService.runTwitterUpdateService();
        imageRepository.loadData();
    }

    @Override
    protected void componentClosed() {
        imageRepository.saveData();
    }
    
     

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(TwitterService.PROPERTY_NAME_TWEET_LIST)) {
            updateTweetList((List<TweetVO>) evt.getNewValue());
        }
    }

    /**
     * Method will update Tweet Panel 
     * @param tweetList 
     */
    private void updateTweetList(List<TweetVO> tweetList) {
        messagePanel.removeAll();
        tweetList.stream().forEach((twitMessage) -> {
            SelectedTwitterCellMessagePanel selectedTwitterCellMessagePanel = new SelectedTwitterCellMessagePanel(twitMessage);
            messagePanel.add(selectedTwitterCellMessagePanel);
        });
        messagePanel.revalidate();
        listScroller.revalidate();
    }

}
