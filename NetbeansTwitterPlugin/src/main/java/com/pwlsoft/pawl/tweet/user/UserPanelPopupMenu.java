/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.user;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import twitter4j.User;

/**
 *
 * @author piotr.pawliszcze
 */
public class UserPanelPopupMenu extends JPopupMenu {

    private static final long serialVersionUID = 1L;

    public UserPanelPopupMenu(User user) {
        JMenuItem showUserTweets = new JMenuItem("Show " + user.getName() + " tweets");
        add(showUserTweets);
        JMenuItem followUser = new JMenuItem("Follow " + user.getName());
//        if(user.isFollowRequestSent() || user.)
//        add(followUser);
    }

}
