/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.factories;

import java.awt.Font;
import twitter4j.HashtagEntity;
import twitter4j.Status;
import twitter4j.URLEntity;
import twitter4j.UserMentionEntity;

/**
 *
 * @author piotr.pawliszcze
 */
public class TextUtils {

    public static final Font DEFAULT_TWEET_FONT = new Font("Consolas", Font.PLAIN, 12);

    public static String bodyRule = "body { font-family: " + DEFAULT_TWEET_FONT.getFamily() + "; "
            + "font-size: " + DEFAULT_TWEET_FONT.getSize() + "pt ; "
            + "background-color: rgb(41,47,51); "
            + "color: rgb(204,214,221)"
            + "}"
            + "a { color : rgb(102,117,127)}"
            + ".date { "
            + "font-size: 10 pt ; "
            + "color : rgb(204,214,221)}"
            +".small {\"h\":238, \"resize\":\"fit\", \"w\":226}";
    
    public static final String twitterLink = "https://twitter.com/";
    private static final String hashTagPrefix = "hashtag/";

    public static String formatTweetText(Status tweet) {
        String tweetMessage = tweet.getText();

        //@user mention
        UserMentionEntity[] userMentionEntities = tweet.getUserMentionEntities();
        for (UserMentionEntity userMentionEntity : userMentionEntities) {
            tweetMessage = tweetMessage.replaceAll("@" + userMentionEntity.getText(), "<a href=\"" + twitterLink + userMentionEntity.getScreenName() + "\">" + "@" + userMentionEntity.getText() + "</a>");
        }

        //HashTag
        HashtagEntity[] hasharray = tweet.getHashtagEntities();
        for (HashtagEntity hashTag : hasharray) {
            tweetMessage = tweetMessage.replaceAll("#" + hashTag.getText(), "<a href=\"" + twitterLink + hashTagPrefix + hashTag.getText() + "\">" + "#" + hashTag.getText() + "</a>");
        }

        //URL
        URLEntity[] urlEntity = tweet.getURLEntities();
        for (URLEntity url : urlEntity) {
            tweetMessage = tweetMessage.replace(url.getText(), "<a href=\"" + url.getText() + "\">" + url.getDisplayURL() + "</a>");
        }
        //Photos
//        MediaEntity[] mediaEntities = tweet.getTwett().getMediaEntities();
//        for (MediaEntity mediaEntity : mediaEntities) {
//            if (mediaEntity.getType().equalsIgnoreCase("photo")) {
//                tweetMessage = tweetMessage.replace(mediaEntity.getText(), "<img src=\"" + mediaEntity.getMediaURL() + "  height=\"120\" >");
//            }
//        }
 //      tweetMessage = tweetMessage +"</br></br><div class=\"date\">"+tweet.getTwett().getCreatedAt()+"<div>";
        return tweetMessage;
    }
}
