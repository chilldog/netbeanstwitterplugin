/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.factories;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import twitter4j.Status;

/**
 *
 * @author piotr.pawliszcze
 */
public class TweetTimestampFactory {

    /**
     * Method translate time in tweet to readable for user formart.
     * @param tweet
     * @return 
     */
    public static String getPublishedTimeStamp(Status tweet) {
        Date tweetDate = tweet.getCreatedAt();
        long milliseconds = Duration.between(tweetDate.toInstant(), Instant.now()).toMillis();
        long toSeconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds);

        //hours
        if(toSeconds > 3600){
              return String.format("%d%s %d%s", TimeUnit.SECONDS.toHours(toSeconds), "h",TimeUnit.SECONDS.toMinutes(toSeconds)-60,"min" );
              
        }
        //minutes
        if (TimeUnit.MILLISECONDS.toSeconds(milliseconds) > 60) {
            return String.format("%d %s", TimeUnit.MILLISECONDS.toMinutes(milliseconds), "min");
        }
        //seconds
        return String.format("%d %s", TimeUnit.MILLISECONDS.toSeconds(milliseconds), "sec");

    }

}
