/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.ui;

/**
 *
 * @author piotr.pawliszcze
 */
public enum ActionButtonType {

    FAVORITIE("favorite"),
    REPEAT_MEDIA("retweet"),
    SHOW_FULL("arrow"),
    COMMENT("reply");

    private final String iconName;

    private ActionButtonType(String icon) {
        this.iconName = icon;
    }

    public String getIconName() {
        return iconName;
    }

    @Override
    public String toString() {
        return iconName;
    }

}
