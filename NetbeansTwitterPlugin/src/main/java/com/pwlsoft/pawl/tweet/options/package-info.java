/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
@OptionsPanelController.ContainerRegistration(id = "Pawl", categoryName = "#OptionsCategory_Name_Pawl", iconBase = "com/pwlsoft/pawl/twit/icons/share_icon_twitter.png", keywords = "#OptionsCategory_Keywords_Pawl", keywordsCategory = "Pawl")
@NbBundle.Messages(value = {"OptionsCategory_Name_Pawl=P@wl", "OptionsCategory_Keywords_Pawl=pawl,twitter"})
package com.pwlsoft.pawl.tweet.options;

import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.NbBundle;
