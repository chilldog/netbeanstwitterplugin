/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.domain;

import java.io.Serializable;
import twitter4j.Status;

/**
 *
 * @author piotr.pawliszcze
 */
 
public class TweetVO implements Serializable {

    private boolean isNew = false;
    private Status tweet;
   

    public TweetVO() {
    }

    public TweetVO(Status twett) {
        this.tweet = twett;
        this.isNew = true;
    }

    public boolean isIsNew() {
        return isNew;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    /**
     * Get the value of twett
     *
     * @return the value of twett
     */
    public Status getTweet() {
        return tweet;
    }

    /**
     * Set the value of twett
     *
     * @param tweet new value of twett
     */
    public void setTweet(Status tweet) {
        this.tweet = tweet;
    }

}
