/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.service;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import org.netbeans.api.annotations.common.StaticResource;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author piotr.pawliszcze
 */
@ServiceProvider(service = IconService.class)
public class IconService {

    private static final Logger LOG = Logger.getLogger(IconService.class.getName());

    private final Map<Long, ImageIcon> iconMap;

    @StaticResource
    private static final String USER_FOLLOW_ME_ICON_PATH = "com/pwlsoft/pawl/twit/icons/followedByMe.png";
    private static final String USER_NOT_FOLLOW_ME_ICON_PATH = "com/pwlsoft/pawl/twit/icons/notFolloweByMe.png";

    public IconService() {
        this.iconMap = new HashMap<>();
    }

    /** 
     * 
     * @return  Icon Service 
     */
    public static IconService getInstance() {
        return Lookup.getDefault().lookup(IconService.class);
    }

    public void addUserIconToRepository(long userId, String iconUrl) {
        SwingUtilities.invokeLater(() -> {
            ImageIcon imageIconFromURL = getImageIconFromURL(iconUrl);
            iconMap.put(userId, imageIconFromURL);
        });

    }

    private ImageIcon getImageIconFromURL(String imageURL) {
        try {
            URL url = new URL(imageURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Image image = ImageIO.read(input);
            connection.disconnect();
            ImageIcon imageIcon = new ImageIcon(image);
            return imageIcon;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ImageIcon getUserFollowMeIcon() {
        return ImageUtilities.loadImageIcon(USER_FOLLOW_ME_ICON_PATH, true);
    }

    public ImageIcon getUserNotFollowMeIcon() {
        return ImageUtilities.loadImageIcon(USER_NOT_FOLLOW_ME_ICON_PATH, true);
    }

}
