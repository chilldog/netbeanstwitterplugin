/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwlsoft.pawl.tweet.outlineview;

import com.pwlsoft.pawl.tweet.controlers.ToolTipController;
import com.pwlsoft.pawl.tweet.domain.TweetVO;
import com.pwlsoft.pawl.tweet.factories.ImageRepository;
import com.pwlsoft.pawl.tweet.factories.TextUtils;
import com.pwlsoft.pawl.tweet.factories.TweetTimestampFactory;
import com.pwlsoft.pawl.tweet.service.TwitterService;
import com.pwlsoft.pawl.tweet.ui.ActionButtonType;
import com.pwlsoft.pawl.tweet.ui.DoubleStateIcon;
import static com.pwlsoft.pawl.tweet.ui.DoubleStateIcon.ICON_PREFIX;
import com.pwlsoft.pawl.tweet.user.TwiterUserViewPanel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.html.HTMLDocument;
import net.java.balloontip.BalloonTip;
import net.java.balloontip.styles.BalloonTipStyle;
import net.java.balloontip.styles.EdgedBalloonStyle;
import net.java.balloontip.utils.FadingUtils;
import org.openide.util.Exceptions;
import twitter4j.Status;
import twitter4j.User;

/**
 *
 * @author piotr.pawliszcze
 */
public class SelectedTwitterCellMessagePanel extends javax.swing.JPanel {

    private final long panelId;
    private static final long serialVersionUID = 1L;
    private TweetVO tweet = null;
    private final TwitterService service;
    private static final String RETWEETED_POSTFIX = " Retweeted";
    private final ImageRepository imageRepository;
    private final int resizePanelValue = 100;
    private int defaultHeightValue = 200;
    private int defaultWidthValue = 250;

    private static final Logger LOG = Logger.getLogger(SelectedTwitterCellMessagePanel.class.getName());

    public SelectedTwitterCellMessagePanel(TweetVO messageVO) {

        panelId = messageVO.getTweet().getId();
        initComponents();
        service = TwitterService.getinstance();
        imageRepository = ImageRepository.getinstance();
        tweet = messageVO;

        Status tweetVO = messageVO.getTweet();
        DoubleStateIcon jlblFavorite = createFavoriteIcon(messageVO);
        DoubleStateIcon jlblComment = createCommentIcon(messageVO);
        DoubleStateIcon jlblRepeatMedia = createRepeatMediaIcon(tweetVO);

        if (tweet.isIsNew()) {
            jlblAuthorField.setForeground(new Color(51, 104, 255));
        }

        if (tweet.getTweet().isRetweet()) {
            jblReetweetedBy.setText(tweetVO.getUser().getName() + RETWEETED_POSTFIX);
            String retweeteduserLink = TextUtils.twitterLink + tweetVO.getUser().getScreenName();
            jblReetweetedBy.addMouseListener(new MouseMover(retweeteduserLink, jblReetweetedBy));
            jblReetweetedBy.setIcon(new ImageIcon(getClass().getResource(ICON_PREFIX + ActionButtonType.REPEAT_MEDIA.getIconName() + "_on.png")));
            tweetVO = messageVO.getTweet().getRetweetedStatus();
        }

        jlblAuthorField.setText(tweetVO.getUser().getName());
        String authorLink = TextUtils.twitterLink + tweetVO.getUser().getScreenName();
        jlblAuthorField.addMouseListener(new MouseMover(authorLink, jlblAuthorField));

        jlblScreenName.setText("@" + tweetVO.getUser().getScreenName());
        jlblScreenName.addMouseListener(new MouseMover(authorLink, jlblAuthorField));
        jlblTweetDate.setText(TweetTimestampFactory.getPublishedTimeStamp(tweetVO));

        jTweetPanel.setEditorKit(JEditorPane.createEditorKitForContentType("text/html"));
        jTweetPanel.addHyperlinkListener((HyperlinkEvent e) -> {
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                if (Desktop.isDesktopSupported()) {
                    try {
                        Desktop.getDesktop().browse(e.getURL().toURI());
                    } catch (IOException | URISyntaxException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        jTweetPanel.setEditable(false);
        jTweetPanel.setOpaque(false);

        ((HTMLDocument) jTweetPanel.getDocument()).getStyleSheet().addRule(TextUtils.bodyRule);
        String formatTweetText = TextUtils.formatTweetText(tweetVO);
         
        addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
//                //Counting rows logic
//                try {
//                    Object source = e.getSource();
//                    LOG.info(source.toString());
//                    //przeliczam tekst
//                    FontMetrics fm = jTweetPanel.getFontMetrics(jTweetPanel.getFont());
//                    String text = jTweetPanel.getDocument().getText(0, jTweetPanel.getDocument().getLength());
//                    
//                    //przeliczam ile wierszy przy danej szerokości edytora
//                    int rows = ((fm.stringWidth(text) + 2)/e.getComponent().getWidth())+1;
//                    
//                    //wysokosc jednej lini tekstu
//                    int textHeight = fm.getHeight();
//                    
//                    //wysokosc edytora 
//                    double panelHeight = jTweetPanel.getPreferredSize().height;
//                    
//                    //ile zostanie miejsca
//                    double leftSpaceInPanel = panelHeight-(textHeight*rows);
//                    
//                    LOG.info(tweet.getTweet().getUser().getScreenName()+ " ilość wierszy : "+rows +" ile zostaniemiejsca : "+(panelHeight-leftSpaceInPanel)+"px" );
//                    Dimension preferredSize = jTweetPanel.getPreferredSize();
//                    if(rows >3){
//                        double paneHeight = preferredSize.getHeight();
//                        Dimension newSize = new Dimension(preferredSize.width, (int) (paneHeight+(textHeight*(rows-3))));
//                        
//                        jTweetPanel.setPreferredSize(newSize);
//                    }else{
//                        double paneHeight = preferredSize.getHeight();
//                        Dimension newSize = new Dimension(preferredSize.width, (int) (preferredSize.width -(paneHeight-(textHeight*rows))));
//                        jTweetPanel.setPreferredSize(newSize);
//                    }
//                SelectedTwitterCellMessagePanel.this.revalidate();
//                SelectedTwitterCellMessagePanel.this.repaint();
// 
//                } catch (BadLocationException ex) {
//                    Exceptions.printStackTrace(ex);
//                }
            }

            @Override
            public void componentMoved(ComponentEvent e) {
//                LOG.info("moved");
            }

            @Override
            public void componentShown(ComponentEvent e) {
                LOG.info("shown");
            }

            @Override
            public void componentHidden(ComponentEvent e) {
                LOG.info("hidden");
            }
        });

        jTweetPanel.setText(formatTweetText);

        //getUserPicture
        final ImageIcon profileImage = imageRepository.getProfileImagesMap().get(tweetVO.getUser().getId());
        final User user = tweetVO.getUser();
//        Objects.requireNonNull(profileImage, "profile image is null for user : " + tweetVO.getUser().getScreenName());
        if (profileImage != null) {
            try {
                SwingUtilities.invokeAndWait(() -> {
                    jlblPicture.setIcon(profileImage);
                    TwiterUserViewPanel twiterUserViewPanel = new TwiterUserViewPanel(user, profileImage, null);
                    createTooltip(jlblAuthorField, twiterUserViewPanel);
                });
            } catch (InterruptedException | InvocationTargetException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

        actionButtonPanel.add(jlblComment);
        actionButtonPanel.add(jlblRepeatMedia);
        actionButtonPanel.add(jlblFavorite);

        DoubleStateIcon settings = new DoubleStateIcon("", ActionButtonType.SHOW_FULL, true) {
            @Override
            public void click(ActionEvent e) {
                SwingUtilities.invokeLater(() -> {
                    Dimension preferredSize = SelectedTwitterCellMessagePanel.this.getPreferredSize();
                    if (preferredSize.height == 250) {
                        SelectedTwitterCellMessagePanel.this.setPreferredSize(new Dimension(preferredSize.width, preferredSize.height - resizePanelValue));
                    } else {
                        SelectedTwitterCellMessagePanel.this.setPreferredSize(new Dimension(preferredSize.width, preferredSize.height + resizePanelValue));
                    }
                    SelectedTwitterCellMessagePanel.this.revalidate();
                    SelectedTwitterCellMessagePanel.this.repaint();
                });
            }
        };

        actionButtonPanel.add(settings);

        revalidate();
        repaint();

    }

    private DoubleStateIcon createRepeatMediaIcon(Status tweetVO) {

        String retweetCount = String.valueOf(tweetVO.getRetweetCount());
        return new DoubleStateIcon(retweetCount, ActionButtonType.REPEAT_MEDIA, false) {
            @Override
            public void click(ActionEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setSelected(boolean b) {
                super.setSelected(tweetVO.isRetweetedByMe()); //To change body of generated methods, choose Tools | Templates.
            }

        };

    }

    private DoubleStateIcon createCommentIcon(TweetVO tweetVO) {
        return new DoubleStateIcon("", ActionButtonType.COMMENT, false) {
            @Override
            public void click(ActionEvent e) {
                tweetVO.getTweet().getCreatedAt();
            }

        };
    }

    private DoubleStateIcon createFavoriteIcon(TweetVO tweetVO) {
        Status tweet = tweetVO.getTweet();
        return new DoubleStateIcon(String.valueOf(tweet.getFavoriteCount()), ActionButtonType.FAVORITIE, tweet.isFavorited()) {
            @Override
            public void click(ActionEvent e) {
                //TODO need
                if (tweet.isFavorited()) {
                    service.createFavorite(tweet.getId());
                } else {
                    service.destroyFavorite(tweet.getId());
                }

            }
        };
    }

//    private final SwingWorker<Long, ImageIcon> loadimages = new SwingWorker<Long, ImageIcon>() {
//
//        @Override
//        protected Long doInBackground() throws Exception {
//            String profileImageURL = tweet.getTwett().getUser().getProfileImageURL();
//            User user = tweet.getTwett().getUser();
//            if (tweet.getTwett().isRetweet()) {
//                profileImageURL = tweet.getTwett().getRetweetedStatus().getUser().getProfileImageURL();
//
//                String retweetedProfileImageURL = tweet.getTwett().getUser().getProfileImageURL();
//                ImageIcon retweetedIconPicture = getImageIconFromURL(retweetedProfileImageURL);
//                TwiterUserViewPanel retweetedTwiterUserViewPanel = new TwiterUserViewPanel(tweet.getTwett().getUser(), retweetedIconPicture, null);
//                user = tweet.getTwett().getRetweetedStatus().getUser();
//                createTooltip(jblReetweetedBy, retweetedTwiterUserViewPanel);
//            }
//            ImageIcon imageIconFromURL = getImageIconFromURL(profileImageURL);
//            SwingUtilities.invokeLater(() -> {
//                jlblPicture.setIcon(imageIconFromURL);
//            });
//
//            TwiterUserViewPanel twiterUserViewPanel = new TwiterUserViewPanel(user, imageIconFromURL, null);
//            createTooltip(jlblAuthorField, twiterUserViewPanel);
//            repaint();
//            return null;
//        }
//
//        @Override
//        protected void process(List<ImageIcon> chunks) {
//            super.process(chunks); //To change body of generated methods, choose Tools | Templates.
//        }
//
//    };
    private void createTooltip(JLabel field, TwiterUserViewPanel twiterUserViewPanel) {
        BalloonTipStyle edgedLook = new EdgedBalloonStyle(new Color(41, 47, 51), new Color(153, 153, 153));
        BalloonTip tooltipBalloon = new BalloonTip(field, twiterUserViewPanel, edgedLook, false);
        FadingUtils.fadeInBalloon(tooltipBalloon, null, 300, 24);
        tooltipBalloon.setVisible(false);
        // Add tooltip behaviour
        ToolTipController tTC = new ToolTipController(tooltipBalloon, 500, 90000);
        tooltipBalloon.getAttachedComponent().addMouseListener(tTC);
        tooltipBalloon.getAttachedComponent().addMouseMotionListener(tTC);
    }

//    private ImageIcon getImageIconFromURL(String profileImageURL) {
//        try {
//            URL url = new URL(profileImageURL);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//            Image image = ImageIO.read(input);
//            connection.disconnect();
//            ImageIcon imageIcon = new ImageIcon(image);
//            image = null;
//            return imageIcon;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }
    /**
     * Creates new form PawlTwitterMessagePanel
     */
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        mainHeaderPanel = new javax.swing.JPanel();
        headerPanel = new javax.swing.JPanel();
        jblReetweetedBy = new javax.swing.JLabel();
        jlblPicture = new javax.swing.JLabel();
        jlblAuthorField = new javax.swing.JLabel();
        jlblScreenName = new javax.swing.JLabel();
        jlblTweetDate = new javax.swing.JLabel();
        jTweetPanel = new javax.swing.JEditorPane();
        jPanel1 = new javax.swing.JPanel();
        commentPanel = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        actionButtonPanel = new javax.swing.JPanel();

        setBackground(new java.awt.Color(41, 47, 51));
        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setMinimumSize(new java.awt.Dimension(292, 132));
        setPreferredSize(new java.awt.Dimension(104, 150));
        setLayout(new java.awt.BorderLayout());

        mainHeaderPanel.setBackground(new java.awt.Color(41, 47, 51));
        mainHeaderPanel.setLayout(new java.awt.BorderLayout());

        headerPanel.setBackground(new java.awt.Color(41, 47, 51));
        headerPanel.setLayout(new java.awt.GridBagLayout());

        jblReetweetedBy.setFont(new java.awt.Font("Consolas", 0, 12)); // NOI18N
        jblReetweetedBy.setForeground(new java.awt.Color(204, 214, 221));
        jblReetweetedBy.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        org.openide.awt.Mnemonics.setLocalizedText(jblReetweetedBy, org.openide.util.NbBundle.getMessage(SelectedTwitterCellMessagePanel.class, "SelectedTwitterCellMessagePanel.jblReetweetedBy.text")); // NOI18N
        jblReetweetedBy.setAlignmentX(30.0F);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        headerPanel.add(jblReetweetedBy, gridBagConstraints);

        jlblPicture.setBackground(new java.awt.Color(51, 51, 51));
        jlblPicture.setForeground(new java.awt.Color(255, 255, 255));
        jlblPicture.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        org.openide.awt.Mnemonics.setLocalizedText(jlblPicture, org.openide.util.NbBundle.getMessage(SelectedTwitterCellMessagePanel.class, "SelectedTwitterCellMessagePanel.jlblPicture.text")); // NOI18N
        jlblPicture.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jlblPicture.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        headerPanel.add(jlblPicture, gridBagConstraints);

        jlblAuthorField.setBackground(new java.awt.Color(51, 51, 51));
        jlblAuthorField.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jlblAuthorField.setForeground(new java.awt.Color(85, 172, 238));
        org.openide.awt.Mnemonics.setLocalizedText(jlblAuthorField, org.openide.util.NbBundle.getMessage(SelectedTwitterCellMessagePanel.class, "SelectedTwitterCellMessagePanel.jlblAuthorField.text")); // NOI18N
        jlblAuthorField.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jlblAuthorField.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        jlblAuthorField.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LAST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        headerPanel.add(jlblAuthorField, gridBagConstraints);

        jlblScreenName.setFont(new java.awt.Font("Consolas", 0, 10)); // NOI18N
        jlblScreenName.setForeground(new java.awt.Color(204, 214, 221));
        org.openide.awt.Mnemonics.setLocalizedText(jlblScreenName, org.openide.util.NbBundle.getMessage(SelectedTwitterCellMessagePanel.class, "SelectedTwitterCellMessagePanel.jlblScreenName.text")); // NOI18N
        jlblScreenName.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jlblScreenName.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jlblScreenName.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.gridheight = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        headerPanel.add(jlblScreenName, gridBagConstraints);

        mainHeaderPanel.add(headerPanel, java.awt.BorderLayout.WEST);

        jlblTweetDate.setBackground(new java.awt.Color(41, 47, 51));
        jlblTweetDate.setFont(new java.awt.Font("Consolas", 0, 12)); // NOI18N
        jlblTweetDate.setForeground(new java.awt.Color(204, 214, 221));
        jlblTweetDate.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        org.openide.awt.Mnemonics.setLocalizedText(jlblTweetDate, org.openide.util.NbBundle.getMessage(SelectedTwitterCellMessagePanel.class, "SelectedTwitterCellMessagePanel.jlblTweetDate.text")); // NOI18N
        jlblTweetDate.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jlblTweetDate.setAlignmentX(5.0F);
        jlblTweetDate.setAlignmentY(5.0F);
        jlblTweetDate.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        mainHeaderPanel.add(jlblTweetDate, java.awt.BorderLayout.EAST);

        add(mainHeaderPanel, java.awt.BorderLayout.NORTH);

        jTweetPanel.setEditable(false);
        jTweetPanel.setBackground(new java.awt.Color(41, 47, 51));
        jTweetPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTweetPanel.setFont(new java.awt.Font("Consolas", 0, 12)); // NOI18N
        jTweetPanel.setForeground(new java.awt.Color(255, 255, 255));
        jTweetPanel.setMinimumSize(new java.awt.Dimension(200, 270));
        jTweetPanel.setPreferredSize(new java.awt.Dimension(250, 250));
        add(jTweetPanel, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(41, 47, 51));
        jPanel1.setLayout(new java.awt.BorderLayout());

        commentPanel.setBackground(new java.awt.Color(41, 47, 51));
        commentPanel.setLayout(new java.awt.BorderLayout());

        jTextField1.setBackground(new java.awt.Color(41, 47, 51));
        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jTextField1.setForeground(new java.awt.Color(204, 204, 204));
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jTextField1.setText(org.openide.util.NbBundle.getMessage(SelectedTwitterCellMessagePanel.class, "SelectedTwitterCellMessagePanel.jTextField1.text")); // NOI18N
        jTextField1.setMinimumSize(new java.awt.Dimension(60, 22));
        jTextField1.setPreferredSize(new java.awt.Dimension(200, 22));
        commentPanel.add(jTextField1, java.awt.BorderLayout.NORTH);

        jPanel1.add(commentPanel, java.awt.BorderLayout.NORTH);

        actionButtonPanel.setBackground(new java.awt.Color(41, 47, 51));
        actionButtonPanel.setMaximumSize(new java.awt.Dimension(450, 20));
        actionButtonPanel.setMinimumSize(new java.awt.Dimension(450, 20));
        actionButtonPanel.setPreferredSize(new java.awt.Dimension(450, 20));
        actionButtonPanel.setLayout(new javax.swing.BoxLayout(actionButtonPanel, javax.swing.BoxLayout.LINE_AXIS));
        jPanel1.add(actionButtonPanel, java.awt.BorderLayout.SOUTH);

        add(jPanel1, java.awt.BorderLayout.SOUTH);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel actionButtonPanel;
    private javax.swing.JPanel commentPanel;
    private javax.swing.JPanel headerPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JEditorPane jTweetPanel;
    private javax.swing.JLabel jblReetweetedBy;
    private javax.swing.JLabel jlblAuthorField;
    private javax.swing.JLabel jlblPicture;
    private javax.swing.JLabel jlblScreenName;
    private javax.swing.JLabel jlblTweetDate;
    private javax.swing.JPanel mainHeaderPanel;
    // End of variables declaration//GEN-END:variables

    public TweetVO getTwett() {
        return tweet;
    }

    public long getPanelId() {
        return panelId;
    }

}

class MouseMover extends MouseAdapter {

    private final String link;
    private final Component c;
    private Font retweetedLabelFont;

    MouseMover(String link, Component c) {
        this.c = c;
        this.link = link;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            if (Desktop.isDesktopSupported()) {
                try {
                    Desktop.getDesktop().browse(new URL(link).toURI());
                } catch (IOException | URISyntaxException e1) {
                    e1.printStackTrace();
                }
            }
        }
        if (SwingUtilities.isRightMouseButton(e)) {

        }

    }

    @Override
    public void mouseEntered(java.awt.event.MouseEvent evt) {
        retweetedLabelFont = c.getFont();
        Map attributes = retweetedLabelFont.getAttributes();
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        c.setFont(retweetedLabelFont.deriveFont(attributes));
    }

    @Override
    public void mouseExited(java.awt.event.MouseEvent evt) {
        c.setFont(retweetedLabelFont);
    }

}
